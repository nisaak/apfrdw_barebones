﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class S2TRedirector : SteerToRedirector {

    public GameObject target;
    

    public override void PickRedirectionTarget()
    {
        currentTarget = target.transform;
    }
}
