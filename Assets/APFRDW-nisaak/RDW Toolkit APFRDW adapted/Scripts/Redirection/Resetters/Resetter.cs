﻿using UnityEngine;
using System.Collections;
using Redirection;

/// <summary>
/// This is the abstract class for handling the resetting. Here we find methods to check for certain conditions, i.e. if a reset is required.
/// Additionally, abstract methods are defined that have to be implemented in subclass
/// </summary>
public abstract class Resetter : MonoBehaviour {

    [HideInInspector]
    public RedirectionManager redirectionManager;
    [HideInInspector]
    RedirectionVector redirectionVector;
    [HideInInspector]
    public GameObject trackedSpace;

    public Vector3 totalForceVector; //relative to redirected user transform

    public bool disableKeyboardController = false; //used this variable to avoid some misfunctions that happened sometimes

    enum Boundary { Top, Bottom, Right, Left };

    float maxX, maxZ;
    bool obstacleTooClose;
    GameObject closestObstacle;


    /// <summary>
    /// Function called when reset trigger is signaled, to see if resetter believes resetting is necessary.
    /// </summary>
    /// <returns></returns>
    public abstract bool IsResetRequired();

    public abstract void InitializeReset();

    public abstract void ApplyResetting();

    public abstract void FinalizeReset();

    //Not relevant for APF-RDW
    public abstract void SimulatedWalkerUpdate();

    private void Awake()
    {
        trackedSpace = GameObject.Find("Tracked Space");
        redirectionVector = GetComponent<RedirectionVector>(); //change name
    }

    private void Start() {
        closestObstacle = null;
    }


    private void Update()
    {
        totalForceVector = redirectionVector.totalForceVector;

        //Debug.Log((getNearestBoundary(), isUserFacingAwayFromWall(), isUserFacingAwayFromNearestObstacle(), IsUserOutOfBounds(), getDistanceToCenter()));
        //Debug.Log((isUserFacingAwayFromNearestObstacle(), IsObstacleTooClose(), getUserAngleWithNearestObstacle()));
    }

    public void InjectRotation(float rotationInDegrees)
    {
        this.transform.RotateAround(Utilities.FlattenedPos3D(redirectionManager.headTransform.position), Vector3.up, rotationInDegrees);
        //Debug.Log(redirectionManager.headTransform.position);
        if (!disableKeyboardController)
            this.GetComponentInChildren<KeyboardController>().SetLastRotation(rotationInDegrees);
        redirectionManager.statisticsLogger.Event_Rotation_Gain_Reorientation(rotationInDegrees / redirectionManager.deltaDir, rotationInDegrees);
    }

    public void Initialize()
    {
        maxX = 0.5f * (redirectionManager.trackedSpaceSizeX) - redirectionManager.resetTrigger.RESET_TRIGGER_BUFFER;// redirectionManager.resetTrigger.xLength);// + USER_CAPSULE_COLLIDER_DIAMETER);
        maxZ = 0.5f * (redirectionManager.trackedSpaceSizeZ) - redirectionManager.resetTrigger.RESET_TRIGGER_BUFFER;


        //print("PRACTICAL MAX X: " + maxX);
    }

    public bool IsUserOutOfBounds()
    {
        return Mathf.Abs(redirectionManager.currPosReal.x) >= maxX || Mathf.Abs(redirectionManager.currPosReal.z) >= maxZ;

    }

    /// <summary>
    /// If tracking space is rectangular, this method gets the user's position and returns the nearest wall (up,down,left,right)
    /// </summary>
    /// <returns></returns>
    Boundary getNearestBoundary()
    {
        Vector3 position = redirectionManager.currPosReal;
        if (position.x >= 0 && Mathf.Abs(maxX - position.x) <= Mathf.Min(Mathf.Abs(maxZ - position.z), Mathf.Abs(-maxZ - position.z))) // for a very wide rectangle, you can find that the first condition is actually necessary
            return Boundary.Right;
        if (position.x <= 0 && Mathf.Abs(-maxX - position.x) <= Mathf.Min(Mathf.Abs(maxZ - position.z), Mathf.Abs(-maxZ - position.z)))
            return Boundary.Left;
        if (position.z >= 0 && Mathf.Abs(maxZ - position.z) <= Mathf.Min(Mathf.Abs(maxX - position.x), Mathf.Abs(-maxX - position.x)))
            return Boundary.Top;
        return Boundary.Bottom;
    }

    /// <summary>
    /// gets direction away from nearest boundary
    /// </summary>
    /// <returns></returns>
    Vector3 getAwayFromNearestBoundaryDirection()
    {
        Boundary nearestBoundary = getNearestBoundary();
        switch (nearestBoundary)
        {
            case Boundary.Top:
                return -Vector3.forward;
            case Boundary.Bottom:
                return Vector3.forward;
            case Boundary.Right:
                return -Vector3.right;
            case Boundary.Left:
                return Vector3.right;
        }
        return Vector3.zero;
    }

    float getUserAngleWithNearestBoundary()
    {
        return Utilities.GetSignedAngle(redirectionManager.currDirReal, getAwayFromNearestBoundaryDirection());
    }

    float getUserAngleWithNearestObstacle()
    {
        float angleToNearestObstacle;
        angleToNearestObstacle = (-1) * Utilities.GetSignedAngle(Utilities.FlattenedPos3D(closestObstacle.transform.localPosition - redirectionManager.currPosReal), redirectionManager.currDirReal);
        Debug.Log(angleToNearestObstacle);
        return angleToNearestObstacle;
;
    }
    protected bool isUserFacingAwayFromWall()
    {
        return Mathf.Abs(getUserAngleWithNearestBoundary()) < 90;
    }

    public bool isUserFacingAwayFromNearestObstacle()
    {
        return Mathf.Abs(getUserAngleWithNearestObstacle()) > 90;
    }

    ///this function checks if the nearest obstacle is too close
    public bool IsObstacleTooClose() 
    {
        GetClosestObstacleGameObject();
        if(closestObstacle == null){
            return false;
        }
        else{
            float sqrDistanceToClosestObstacle = Utilities.FlattenedPos3D(redirectionManager.currPosReal - closestObstacle.transform.localPosition).sqrMagnitude;
            float maxAllowedDistanceToObstacle = 0.5f;
            if(sqrDistanceToClosestObstacle != 0)
                return (sqrDistanceToClosestObstacle < maxAllowedDistanceToObstacle * maxAllowedDistanceToObstacle);
            return false;
        }
    }

    public float getTrackingAreaHalfDiameter()
    {
        return Mathf.Sqrt(maxX * maxX + maxZ * maxZ);
    }

    public float getDistanceToCenter()
    {
        return Utilities.FlattenedPos3D(redirectionManager.currPosReal).magnitude;
    }

    public void GetClosestObstacleGameObject(){
        float minLength = float.MaxValue;
        if(SceneInformation.dynamicObstacles.Length + SceneInformation.staticObstacles.Length == 0){}
        else{
            for (int i = 0; i < SceneInformation.dynamicObstacles.Length + SceneInformation.staticObstacles.Length; i++)
            {
                if (i < SceneInformation.dynamicObstacles.Length)
                {
                    Vector3 shortestVectorToObstacle = (Utilities.FlattenedPos3D(SceneInformation.dynamicObstacles[i].transform.localPosition) - Utilities.FlattenedPos3D(redirectionManager.currPosReal));
                    float shortestVectorToObstacleLength = shortestVectorToObstacle.magnitude;
                    minLength = Mathf.Min(shortestVectorToObstacleLength, minLength);
                    if (minLength == shortestVectorToObstacleLength) closestObstacle = SceneInformation.dynamicObstacles[i];
                }
                else
                {
                    Vector3 shortestVectorToObstacle = (Utilities.FlattenedPos3D(SceneInformation.staticObstacles[i- SceneInformation.dynamicObstacles.Length].transform.localPosition) - Utilities.FlattenedPos3D(redirectionManager.currPosReal));
                    float shortestVectorToObstacleLength = shortestVectorToObstacle.magnitude;
                    minLength = Mathf.Min(shortestVectorToObstacleLength, minLength);
                    if(minLength == shortestVectorToObstacleLength) closestObstacle = SceneInformation.staticObstacles[i - SceneInformation.dynamicObstacles.Length];
                }
            }
        }
    }

    public float getDistanceToNearestBoundary()
    {
        Vector3 position = redirectionManager.currPosReal;
        Boundary nearestBoundary = getNearestBoundary();
        switch (nearestBoundary)
        {
            case Boundary.Top:
                return Mathf.Abs(maxZ - position.z);
            case Boundary.Bottom:
                return Mathf.Abs(-maxZ - position.z);
            case Boundary.Right:
                return Mathf.Abs(maxX - position.x);
            case Boundary.Left:
                return Mathf.Abs(-maxX - position.x);
        }
        return 0;
    }

    public float getMaxWalkableDistanceBeforeReset()
    {
        Vector3 position = redirectionManager.currPosReal;
        Vector3 direction = redirectionManager.currDirReal;
        float tMaxX = direction.x != 0 ? Mathf.Max((maxX - position.x) / direction.x, (-maxX - position.x) / direction.x) : float.MaxValue;
        float tMaxZ = direction.z != 0 ? Mathf.Max((maxZ - position.z) / direction.z, (-maxZ - position.z) / direction.z) : float.MaxValue;
        //print("MaxX: " + maxX);
        //print("MaxZ: " + maxZ);
        return Mathf.Min(tMaxX, tMaxZ);
    }

}
