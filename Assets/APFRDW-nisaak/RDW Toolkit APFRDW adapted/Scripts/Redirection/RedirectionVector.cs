﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Redirection;

public class RedirectionVector : MonoBehaviour
{
    //Provide variables for totalForceVector relative to World space
    public Vector3 totalForceVector;

    Vector3 obst_dist;

    //reference to unscaled origin point, obsolete
    GameObject origin; 

    //references to obstacles, redirectionmanager script
    GameObject[] obstacles;
    RedirectionManager redirectionManager;
    [Range(0.1f,2f)]
    public float pow = 1.25f;


    bool check = false;

    public void CalcTFV()
    {
        //initialise tfv to zero
        totalForceVector = Vector3.zero;
        foreach (GameObject obstacle in obstacles)
        {
            //obstacles relative to redirected user origin
            obst_dist = redirectionManager.currPosReal - obstacle.transform.localPosition; 

            //get distance
            float obst_dist_mag = obst_dist.magnitude;
            totalForceVector += new Vector3(obst_dist.x / 2*(Mathf.Pow(obst_dist_mag,pow)), 0.0f, obst_dist.z / 2*(Mathf.Pow(obst_dist_mag,pow)));

        }

        //normalize length
        totalForceVector.Normalize();
    }

    //create tag list to go through all obstacles
    public void createTagList()
    {
        GameObject[] FindGameObjectsWithTags(params string[] tags) //local method to provide more than one tag
        {
            var all = new List<GameObject>();

            foreach (string tag in tags)
            {
                all.AddRange(GameObject.FindGameObjectsWithTag(tag).ToList());
            }

            return all.ToArray();
        }


        string[] taglist = new string[2] { "dynamic obstacle", "static obstacle" }; //define tags
        obstacles = FindGameObjectsWithTags(taglist); //apply method
    }


    private void Awake()
    {
        redirectionManager = GetComponentInParent<RedirectionManager>();
    }
    void Start()
    {
    }

    void Update()
    {
        if (!check)
        {
            createTagList();
            check = true;
        }
        else 
        {
            CalcTFV();
        }
    }
}

